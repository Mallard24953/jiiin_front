/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  AppBar,
  Box,
  Drawer,
  IconButton,
  SwipeableDrawer,
  Toolbar,
  Typography,
  useScrollTrigger,
  useTheme,
} from '@mui/material'
import { useContext, useState } from 'react'
import { Outlet, useNavigate } from 'react-router-dom'
import { ColorModeContext } from '../utils/MUIWrapper'
import Brightness4Icon from '@mui/icons-material/Brightness4'
import Brightness7Icon from '@mui/icons-material/Brightness7'
import ChangeLanguage from '../components/ui/ChangeLanguage'
import { Menu } from '@mui/icons-material'
import DrawerMenu from '../components/ui/DrawerMenu'
import UserAvatar from '../components/UserAvatar'
import Footer from '../components/Footer'

const drawerWidth = 280
interface Props {
  window?: () => Window
}

function MainLayout(props: Props) {
  const theme = useTheme()
  const muiUtils = useContext(ColorModeContext)

  const navigate = useNavigate()

  const iOS =
    typeof navigator !== 'undefined' &&
    /iPad|iPhone|iPod/.test(navigator.userAgent)

  const { window } = props
  const container =
    window !== undefined ? () => window().document.body : undefined

  const [mobileOpen, setMobileOpen] = useState(false)
  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState)
  }
  const trigger = useScrollTrigger()

  return (
    <Box
      sx={{
        display: 'flex',
        backgroundColor: theme.palette.background.default,
      }}
    >
      <AppBar
        position='fixed'
        elevation={0}
        color='default'
        sx={{
          zIndex: (theme) => theme.zIndex.drawer - 1,
          ml: { sm: `${drawerWidth}px` },
          boxShadow: trigger ? theme.shadows[2] : 'none',
        }}
      >
        <Toolbar sx={{ height: '80px' }}>
          <Box sx={{ display: { sm: 'flex', md: 'none' } }}>
            <IconButton
              size='large'
              edge='start'
              aria-label='open drawer'
              sx={{ marginInlineEnd: 2 }}
              onClick={handleDrawerToggle}
            >
              <Menu />
            </IconButton>
          </Box>

          <Typography variant='h6' component='div' sx={{ flexGrow: 1 }}>
            JIIIN
          </Typography>

          <Box sx={{ display: 'flex', }}>
            
            <ChangeLanguage />

            <IconButton
              sx={{ fontSize: '1rem' }}
              onClick={muiUtils.toggleColorMode}
              color='inherit'
              disableTouchRipple
              disableRipple
            >
              {theme.palette.mode === 'dark' ? (
                <Brightness7Icon />
              ) : (
                <Brightness4Icon />
              )}
            </IconButton>

            <UserAvatar />
          </Box>
        </Toolbar>
      </AppBar>

      {/* Drawers */}
      <Box
        component='nav'
        sx={{ width: { md: drawerWidth }, flexShrink: { md: 0 } }}
        aria-label='main menu'
      >
        <SwipeableDrawer
          container={container}
          variant='temporary'
          open={mobileOpen}
          onOpen={handleDrawerToggle}
          onClose={handleDrawerToggle}
          disableBackdropTransition={!iOS}
          disableDiscovery={iOS}
          ModalProps={{
            keepMounted: true,
          }}
          sx={{
            display: { xs: 'block', sm: 'block', md: 'none' },
            '& .MuiDrawer-paper': {
              boxSizing: 'border-box',
              width: drawerWidth,
              padding: '16px',
              backgroundColor: theme.palette.background.default,
            },
          }}
        >
          <DrawerMenu />
        </SwipeableDrawer>

        <Drawer
          anchor={'left'}
          variant='permanent'
          sx={{
            display: { xs: 'none', sm: 'none', md: 'block', p: '16px' },
            '& .MuiDrawer-paper': {
              boxSizing: 'border-box',
              width: drawerWidth,
              padding: '16px',
              paddingTop: 0,
              backgroundColor: theme.palette.background.default,
              border: 'none',
            },
          }}
          open
        >
          <DrawerMenu />
        </Drawer>
      </Box>

      {/* Main */}
      <Box component='main' sx={{ flexGrow: 1, p: 3 }}>
        <Toolbar />
        <Box>
          <Outlet />
        </Box>
        <Footer />
      </Box>

        
    </Box>
  )
}

export default MainLayout
