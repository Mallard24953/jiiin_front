import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from 'i18next-browser-languagedetector'

import english from './translations/english.json'
import russian from './translations/russian.json'
import hebrew from './translations/hebrew.json'

const resources = {
  en: {
    translation: english
  },
  ru: {
    translation: russian
  },
  he: {
    translation: hebrew
  }
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .use(LanguageDetector)
  .init({
    resources,
    fallbackLng: 'ru',
    debug: false,
    detection: {
      order: ['localStorage', 'cookie'],
      caches: ['localStorage', 'cookie']
    },
    interpolation: {
      escapeValue: false,
    },
    react: {
      transKeepBasicHtmlNodesFor: ['br', 'strong', 'i', 'a'],
    }
  });

  export default i18n;