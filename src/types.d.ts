export type ILocation = {
  id?: number
  name: string
  address: string
  phone: string
  timezone?: number
  appointments?: unknown
  cash?: unknown
  cash_flows?: unknown
  storage?: unknown
  item_flows?: undefined
  services?: unknown
  schedules?: unknown
}