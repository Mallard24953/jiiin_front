/* eslint-disable @typescript-eslint/no-unused-vars */

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Theme,
} from '@mui/material'
import CloseIcon from '@mui/icons-material/Close'

interface IModalDialogProps {
  open: boolean
  title: string
  showActions: boolean
  onSubmit?: () => void
  onCancel: () => void
  children?: React.ReactNode
}

function ModalDialog({
  open,
  title,
  showActions,
  onSubmit,
  onCancel,
  children,
}: IModalDialogProps) {
  return (
    <Dialog
      fullWidth
      maxWidth={'sm'}
      open={open}
      onClose={onCancel}
      aria-labelledby='alert-dialog-title'
      aria-describedby='alert-dialog-description'
    >
      <DialogTitle sx={{ mt: 1 }} id='alert-dialog-title'>
        {title}
        <IconButton
          aria-label='close'
          onClick={onCancel}
          sx={{
            position: 'absolute',
            right: 16,
            top: 16,
            color: (theme: Theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent >{children}</DialogContent>
      {showActions && 
      <DialogActions sx={{ px: 3, pb: 3 }}>
        <Button  variant="outlined" onClick={onCancel}>
          Cancel
        </Button>
        <Button color="error" variant="contained" onClick={onSubmit}>
          Confirm
        </Button>
      </DialogActions>
      }
    </Dialog>
  )
}

export default ModalDialog
