/* eslint-disable @typescript-eslint/no-unused-vars */
import { Box, Typography } from '@mui/material'
import { useTranslation } from 'react-i18next'
import { useTheme } from '@mui/material/styles'

function Footer() {
  const theme = useTheme()
  const { t } = useTranslation()
  const appMode = import.meta.env.MODE

  return (
    <Box
    component='footer'
    sx={{
      textAlign: 'right',
      px: 3,
      py: 1,
      mt: 'auto',
    }}
  >
    <Typography color={theme.palette.text.disabled} variant='subtitle2'>
      JIIIN, 2023 {appMode}
    </Typography>
  </Box>
  )
}

export default Footer