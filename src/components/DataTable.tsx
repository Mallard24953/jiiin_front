/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  DataGrid,
  GridColDef,
  GridRowsProp,
  ruRU,
  enUS,
  heIL,
  GridToolbarContainer,
  GridToolbarColumnsButton,
  GridToolbarFilterButton,
} from "@mui/x-data-grid";
import { useLocalStorage } from "../hooks/useLocalStorage";
import i18next from "i18next";

interface DataGridProps {
  columns: GridColDef[];
  rows?: GridRowsProp;
  table: string;
  loading?: boolean;
}

function DataTable({ ...props }: DataGridProps) {
  const table: string = props.table;
  const columns: GridColDef[] = props.columns;
  const rows: GridRowsProp | undefined = props?.rows;

  const [tableFields, setTableFields] = useLocalStorage(table + "_cols", {});
  const [filterModel, setFilterModel] = useLocalStorage(table + "_filters", {})

  const saveCols = (e: unknown) => {
    setTableFields(e);
  };

  const saveFilters = (e: unknown) => {
    console.log(e)
    setFilterModel(e)
  }

  function CustomToolbar() {
    return (
      <GridToolbarContainer>
        <GridToolbarColumnsButton />
        <GridToolbarFilterButton />
      </GridToolbarContainer>
    );
  }

  let localeText = ruRU.components.MuiDataGrid.defaultProps.localeText
  if (i18next.language === 'en') {
    localeText = enUS.components.MuiDataGrid.defaultProps.localeText
  }
  if (i18next.language === 'he') {
    localeText = heIL.components.MuiDataGrid.defaultProps.localeText
  }

  return (
    <div style={{ height: 515, width: "100%" }}>
      {rows && (
        <DataGrid
          // getEstimatedRowHeight={() => 100}
          autoHeight={true}
          hideFooterSelectedRowCount={true}
          localeText={localeText}
          // loading={props.loading}
          columnVisibilityModel={tableFields}
          onColumnVisibilityModelChange={(e) => saveCols(e)}
          filterModel={filterModel}
          onFilterModelChange={(e) => saveFilters(e)}
          editMode="row"
          rows={rows}
          columns={columns}
          slots={{
            toolbar: CustomToolbar
          }}
          initialState={{
            filter: {}
          }}
        />
      )} 
    </div>
  );
}

export default DataTable;
