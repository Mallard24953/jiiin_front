import React from "react";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import { useMatch } from "react-router-dom";
import { Link as RouterLink } from "react-router-dom";
import { Link, ListItemButton, Typography } from "@mui/material";
import { styled } from "@mui/material/styles";
import { useTheme } from "@mui/material/styles";

interface CustomNavlinkProps {
  to: string
  iconName?: React.ReactElement
  children: React.ReactNode
}


function CustomNavlink({ ...props }: CustomNavlinkProps) {
  const theme = useTheme();
  const active = useMatch({
    path: props.to,
    end: props.to.length === 1,
  })
    ? true
    : false;

  let icon;
  if (props.iconName) {
    icon = React.cloneElement(
      props.iconName,
      { htmlColor: activeColor() },
      null
    );
  }

  const MyListItemButton = styled(ListItemButton)(() => ({
    borderRadius: "8px",
    height: "34px",
    marginBottom: "2px",
  }));

  function activeColor() {
    return active ? "" : '';
  }

  return (
    <Link underline="none" component={RouterLink} to={props.to}>
      <MyListItemButton
        selected={active}
        sx={[
          { "&:hover": { background: theme.palette.action.hover } },
          active && {
            background: `${theme.palette.primary.main}`,
            color: `${theme.palette.text.primary}`
          },
        ]}
      >
        {icon && (
          <ListItemIcon color="white" sx={{ minWidth: "40px" }}>
            {icon}
          </ListItemIcon>
        )}
        <ListItemText sx={{ ml: 1 }}>
          <Typography variant="body2">
            <span style={{ color: activeColor() }}>{props.children}</span>
          </Typography>
        </ListItemText>
      </MyListItemButton>
    </Link>
  );
}

export default CustomNavlink;
