import React from 'react'
import { Paper } from '@mui/material'
import { styled } from "@mui/material/styles";


interface StyledPaperProps {
  children?: React.ReactNode
}


const StyledPaper = ({...props}:StyledPaperProps ) => {

  const StyledPaper = styled(Paper)(({ theme }) => ({
    borderRadius: "10px",
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[1],
    padding: '10px'
    // color: 'inherit'
  }));


  return (
    <StyledPaper {...props}
    >
      {props.children}
    </StyledPaper>
  )
}

export default StyledPaper