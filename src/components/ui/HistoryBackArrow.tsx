/* eslint-disable @typescript-eslint/no-unused-vars */
import KeyboardBackspaceOutlinedIcon from "@mui/icons-material/KeyboardBackspaceOutlined";
import { useNavigate } from "react-router-dom";
import { Box, Button } from "@mui/material";
import i18next from '../../i18next';
import { useTranslation } from "react-i18next";
import { useThemeMode } from "../../hooks/MUIWrapper";



function HistoryBackArrow() {
  const { theme } = useThemeMode()
  const { t } = useTranslation();
  const navigate = useNavigate();

  const dir = i18next.dir(i18next.resolvedLanguage) 

  return (
    <Box sx={{ display: "flex", alignItems: "center" }}>
      <Box>
        <Button
          sx={{ }}
          onClick={() => {
            navigate(-1);
          }}
          variant="text"
          startIcon={dir === 'rtl'?  <KeyboardBackspaceOutlinedIcon sx={{ transform: 'rotate(180deg)', ml:1}} />:<KeyboardBackspaceOutlinedIcon sx={{}} />}
        >
           {t("button-history_back")}
        </Button>
      </Box>
    </Box>
  );
}

export default HistoryBackArrow;
