/* eslint-disable @typescript-eslint/no-unused-vars */
import { useContext, useState } from "react";
import { ColorModeContext } from "../../utils/MUIWrapper";
import { Box, Direction, IconButton, Menu, MenuItem, useTheme } from "@mui/material";
import { LanguageOutlined } from '@mui/icons-material';
import { changeLanguage } from 'i18next';



function ChangeLanguage() {
  const muiUtils = useContext(ColorModeContext);

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null)
  };

  function changeLang(lang: string){
    setAnchorEl(null)
    changeLanguage(lang)

    if (lang === 'he') {
      muiUtils.changeDirection('rtl' as Direction);
    } else {
      muiUtils.changeDirection('ltr' as Direction);
    }

  }

  return (
    <Box sx={{display:'flex', alignItems: 'center'}}>
    <IconButton
      aria-controls={open ? "change-lang-menu" : undefined}
      aria-haspopup="true"
      aria-expanded={open ? "true" : undefined}
      onClick={handleClick}
    >
      <LanguageOutlined />
    </IconButton>
      {/* { i18next.language } */}
      <Menu
      id="change-lang-menu"
      anchorEl={anchorEl}
      open={open}
      onClose={handleClose}
      MenuListProps={{
        "aria-labelledby": "change-lang-button",
      }}>
      <MenuItem onClick={() => changeLang('ru')}>Русский</MenuItem>
      <MenuItem onClick={() => changeLang('en')}>English</MenuItem>
      <MenuItem onClick={() => changeLang('he')}>עִברִית</MenuItem>
    </Menu>
  </Box>
  )
}

export default ChangeLanguage