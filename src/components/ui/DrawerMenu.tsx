/* eslint-disable @typescript-eslint/no-unused-vars */
import { Divider, List, Toolbar, Typography, useTheme } from '@mui/material'
import { useNavigate } from 'react-router-dom'

import CustomListSubheader from './CustomListSubheader'
import CustomNavlink from './CustomNavLink'
import { useTranslation } from 'react-i18next'

export default function DrawerMenu() {
  const { t } = useTranslation()

  const drawer = (
    <>
      <Toolbar sx={{ height: '80px' }}>
        <Typography variant='h6' component='div' sx={{ flexGrow: 1 }}>
          JIIIN
        </Typography>
      </Toolbar>

      <List
        sx={{ pb: 0 }}
        subheader={
          <CustomListSubheader title='Обслуживание'>
            <CustomNavlink to='/'>{t('menu-timetable-link')}</CustomNavlink>
            <CustomNavlink to='/clients'>{t('menu-clients-link')}</CustomNavlink>
            <CustomNavlink to='/notices'>{t('menu-notices-link')}</CustomNavlink>
            <CustomNavlink to='/cash_flow'>{t('menu-cash_flow-link')}</CustomNavlink>
          </CustomListSubheader>
        }
      ></List>
      <Divider />
      <List
        sx={{ pb: 0 }}
        subheader={
          <CustomListSubheader title={t('menu-company-subheader')}>
            <CustomNavlink to='/schedules'>{t('menu-schedules-link')}</CustomNavlink>
            <CustomNavlink to='/locations'>{t('menu-locations-link')}</CustomNavlink>
            <CustomNavlink to='/services'>{t('menu-services-link')}</CustomNavlink>
            <CustomNavlink to='/staff'>{t('menu-staff-link')}</CustomNavlink>
            <CustomNavlink to='/holidays'>{t('menu-holidays-link')}</CustomNavlink>
            <CustomNavlink to='/tags'>{t('menu-tags-link')}</CustomNavlink>
          </CustomListSubheader>
        }
      ></List>
      <Divider />
      <List
        sx={{ pb: 0 }}
        subheader={
          <CustomListSubheader title={t('menu-price-subheader')}>
            <CustomNavlink to='/schedules'>{t('menu-price-link')}</CustomNavlink>
          </CustomListSubheader>
        }
      ></List>
      <Divider />
      <List
        sx={{ pb: 0 }}
        subheader={
          <CustomListSubheader title={t('menu-quick_start-subheader')}>
            <CustomNavlink to='/schedules'>{t('menu-quick_start-link')}</CustomNavlink>
          </CustomListSubheader>
        }
      ></List>
      <Divider />
      <List
        sx={{ pb: 0 }}
        subheader={
          <CustomListSubheader title={t('menu-settings-subheader')}>
            <CustomNavlink to='/schedules'>{t('menu-settings-link')}</CustomNavlink>
          </CustomListSubheader>
        }
      ></List>
    </>
  )

  return drawer
}
