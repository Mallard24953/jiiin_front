/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from "react";
import {
  Collapse,
  ListItemButton,
  ListItemText,
  Typography,
  useTheme,
} from "@mui/material";

import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { useLocalStorage } from "../../hooks/useLocalStorage";


interface CustomListSubheaderProps {
  title: string
  children: React.ReactNode
}

function CustomListSubheader({ title, children }: CustomListSubheaderProps) {
  const theme = useTheme()

  const menuName = "menu_" + title;

  const [menu, setMenu] = useLocalStorage(menuName, true);

  const [open, setOpen] = useState(menu);
  const handleClick = () => {
    setOpen(!open);
  };

  const saveMenu = () => {
    setMenu(open);
  };

  return (
    <>
      <ListItemButton sx={{ px: "12px" }} onClick={handleClick} dense>
        <ListItemText
          primary={
            <Typography
              sx={{ textTransform: "uppercase", marginBottom: "3px" }}
              variant="caption"
              color={theme.palette.text.secondary}
            >
              {title}
            </Typography>
          }
        />
        {open ? (
          <ExpandLessIcon fontSize="small" />
        ) : (
          <ExpandMoreIcon fontSize="small" />
        )}
      </ListItemButton>
      <Collapse
        in={open}
        timeout="auto"
        unmountOnExit
        addEndListener={() => saveMenu()}
      >
        {children}
      </Collapse>
    </>
  );
}

export default CustomListSubheader;
