import Axios from 'axios'

const access_token = ('access_token')


const axiosAPI = Axios.create({
  baseURL: import.meta.env.VITE_PUBLIC_BACKEND_URL,
  headers: {
    'content-type': 'application/json',
    'Accept': 'application/json',
    'Authorization': `Bearer ${access_token}`,
  },
  withCredentials: true
})

export default axiosAPI
