/* eslint-disable @typescript-eslint/no-unused-vars */
import { createContext, useEffect, useMemo, useState } from 'react'

import {
  PaletteMode,
  createTheme,
  useMediaQuery,
  ThemeProvider,
  Direction,
} from '@mui/material'
import rtlPlugin from 'stylis-plugin-rtl'
import { prefixer } from 'stylis'
import { CacheProvider } from '@emotion/react'
import createCache from '@emotion/cache'

import { useLocalStorage } from '../hooks/useLocalStorage'
import i18next from '../i18next';


export const ColorModeContext = createContext({
  toggleColorMode: () => {},
  changeDirection: (dir: Direction) => {},
})

// Create rtl cache
const cacheRtl = createCache({
  key: 'muirtl',
  stylisPlugins: [prefixer, rtlPlugin],
})

const emptyCache = createCache({
  key: 'meaningless-key',
})

export default function MUIWrapper({
  children,
}: {
  children: React.ReactNode
}) {
  const [clientMode, setClientMode] = useLocalStorage('theme', 'system')
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)')
  const [mode, setMode] = useState<PaletteMode>('dark')
  const clientDir = i18next.dir()
 
  const [direction, setDirection] = useState<Direction>(clientDir)

  useEffect(() => {
    if (!clientMode || clientMode === 'system') {
      setMode(prefersDarkMode ? 'dark' : 'light')
    } else {
      setMode(clientMode)
    }
  }, [clientMode, prefersDarkMode])

  const theme = useMemo(
    () =>
      createTheme({
        direction: direction,
        palette: {
          mode: mode === 'dark' ? 'dark' : 'light',
        },
      }),
    [mode, direction]
  )

  const colorMode = useMemo(
    () => ({
      toggleColorMode: () => {
        setMode((prevMode: PaletteMode) => {
          if (prevMode === 'light') {
            setClientMode('dark')
            return 'dark'
          } else {
            setClientMode('light')
            return 'light'
          }
        }
        )
      },
      changeDirection: (dir: Direction) => {
        setDirection(dir)
      },
    }),
    [setClientMode]
  )

  useEffect(() => {
    document.dir = direction
  }, [direction])

  return (
    <CacheProvider value={direction === 'rtl' ? cacheRtl : emptyCache}>
      <ColorModeContext.Provider value={colorMode}>
        <ThemeProvider theme={theme}>{children}</ThemeProvider>
      </ColorModeContext.Provider>
    </CacheProvider>
  )
}
