import { Routes, Route } from "react-router-dom";
import MainLayout from "../layouts/MainLayout";
import TimeTablePage from "../pages/TimeTable/TimeTablePage";
import SchedulesPage from "../pages/Schedules/SchedulesPage";
import ClientsPage from "../pages/Clients/ClientsPage";
import NoticesPage from "../pages/Notices/NoticesPage";
import CacheDeskPage from "../pages/CacheDesk/CacheDeskPage";
import LocationsPage from "../pages/Locations/LocationsPage";


function AppRoutes() {
  return (
    <Routes>
      <Route path="/" element={<MainLayout />}>
        <Route index element={<TimeTablePage />} />
        <Route path='/clients' element={<ClientsPage />} />
        <Route path='/notices' element={<NoticesPage />} />
        <Route path='/cash_flow' element={<CacheDeskPage />} />

        <Route path='/schedules' element={<SchedulesPage />} />
        <Route path='/locations' element={<LocationsPage />} />
      </Route>
    </Routes>
  );
}
export default AppRoutes;
