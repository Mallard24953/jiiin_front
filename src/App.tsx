/* eslint-disable @typescript-eslint/no-unused-vars */
import CssBaseline from '@mui/material/CssBaseline'
import { BrowserRouter } from 'react-router-dom'
import AppRoutes from './routes/Router'
import MUIWrapper from './utils/MUIWrapper'
import QueryClientWrapper from './utils/QueryClientWrapper'
import { SnackbarProvider } from 'notistack'

function App() {
  return (
    <QueryClientWrapper>
      <MUIWrapper>
        <CssBaseline enableColorScheme />
        <SnackbarProvider
          preventDuplicate
          autoHideDuration={3000}
          anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        >
          <BrowserRouter>
            <AppRoutes />
          </BrowserRouter>
        </SnackbarProvider>
      </MUIWrapper>
    </QueryClientWrapper>
  )
}

export default App
