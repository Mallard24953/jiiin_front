/* eslint-disable @typescript-eslint/no-unused-vars */

import { Box, Grid, Typography } from '@mui/material'
import { useTranslation } from 'react-i18next'
import { useTheme } from '@mui/material/styles'
import StyledPaper from '../../components/ui/StyledPaper'

function TimeTablePage() {
  const theme = useTheme()
  const { t } = useTranslation()

  return (
    <Grid container spacing={3}>
      <Grid
        item
        xs={12}
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          mt: 3
        }}
      >
        <Box>
          <Typography color={theme.palette.text.secondary} variant='h4'>
            {t('index-page_title')}
          </Typography>
        </Box>
      </Grid>
      <Grid item xs={12}>
        <StyledPaper>TimeTable Page</StyledPaper>
      </Grid>
    </Grid>
  )
}

export default TimeTablePage
