/* eslint-disable @typescript-eslint/no-unused-vars */

import {
  Box,
  Button,
  FormControl,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Typography,
} from '@mui/material'
import { useTranslation } from 'react-i18next'
import { useTheme } from '@mui/material/styles'
import StyledPaper from '../../components/ui/StyledPaper'
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import { deleteLocationMutation, getLocationsQuery, getLocationByIDQuery } from '../../api/locationsAPI'
import DataTable from '../../components/DataTable'
import { GridColDef, GridRenderCellParams } from '@mui/x-data-grid'
import SearchIcon from '@mui/icons-material/Search'
import ClearIcon from '@mui/icons-material/Clear'
import { useMemo, useState } from 'react'
import ModalDialog from '../../components/ModalDialog'
import LocationForm from './LocationForm'

import DeleteTwoToneIcon from '@mui/icons-material/DeleteTwoTone'
import EditTwoToneIcon from '@mui/icons-material/EditTwoTone'

function LocationsPage() {
  const theme = useTheme()
  const { t } = useTranslation()
  const queryClient = useQueryClient()
  const [searchString, setSearchString] = useState('')
  const [addModalOpen, setAddModalOpen] = useState(false)
  const [editModalOpen, setEditModalOpen] = useState(false)
  const [deleteModalOpen, setDeleteModalOpen] = useState(false)
  const [locationToDelete, setLocationToDelete] = useState<string>('')
  const [locationToEdit, setLocationToEdit] = useState<string>('')

  const deleteLocation = useMutation({
    mutationFn: deleteLocationMutation,
    retry: 3,
    onSuccess(data, variables, context) {
      queryClient.invalidateQueries({queryKey: ['locations']})
      setDeleteModalOpen(false)
    }, 
    onError(error, variables, context) {
        
    },
  })


  const { data: dataLocations } = useQuery({
    queryKey: ['locations'],
    queryFn: getLocationsQuery,
  })
  const { data: editedLocation } = useQuery({
    queryKey: [`location/${locationToEdit}`],
    queryFn: () => getLocationByIDQuery(locationToEdit),
    enabled: locationToEdit?true:false
  })

  const setSearchQuery = (str: string) => {
    setSearchString(str)
  }
  const clearQuery = () => {
    setSearchString('')
  }

  const filteredData = useMemo(() => {
    return dataLocations?.filter((item) =>
      item.name.toLowerCase().includes(searchString)
    )
  }, [dataLocations, searchString])

  const columns: GridColDef[] = [
    { field: 'name', headerName: t('name'), width: 200 },
    { field: 'address', headerName: t('address'), width: 250 },
    { field: 'phone', headerName: t('phone'), width: 150 },
    {
      field: 'edit',
      headerName: '',
      width: 1,
      align: 'right',
      sortable: false,
      disableColumnMenu: true,
      renderCell: (params: GridRenderCellParams) => (
        <IconButton
          onClick={() => onEdit(params.row.id)}
          aria-label='delete'
          color='success'
          size='small'
        >
          <EditTwoToneIcon />
        </IconButton>
      ),
    },
    {
      field: 'delete',
      headerName: '',
      width: 1,
      align: 'right',
      sortable: false,
      disableColumnMenu: true,
      renderCell: (params: GridRenderCellParams) => (
        <IconButton
          onClick={() => onDelete(params.row.id)}
          aria-label='delete'
          color='error'
          size='small'
        >
          <DeleteTwoToneIcon />
        </IconButton>
      ),
    },
  ]

  const onDelete = (id:string) => {
    setLocationToDelete(id)
    setDeleteModalOpen(true)
  }

  const onDeleteLocation = () => {
    deleteLocation.mutate(locationToDelete)
  }

  const onEdit = (id:string) => {
    setLocationToEdit(id)
    setEditModalOpen(true)
  }

  return (
    <Grid container spacing={3}>
      <Grid
        item
        xs={12}
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          mt: 3,
        }}
      >
        <Box>
          <Typography color={theme.palette.text.secondary} variant='h4'>
            {t('page-locations_title')}
          </Typography>
        </Box>
      </Grid>
      <Grid item xs={12}>
        <StyledPaper>
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              mb: 2,
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <Box>
              <FormControl
                sx={{ my: 1, width: '25ch' }}
                variant='outlined'
                component={'div'}
              >
                <InputLabel htmlFor='search-issue'>
                  {t('search-input.placeholder')}
                </InputLabel>
                <OutlinedInput
                  name='search'
                  autoFocus
                  size='small'
                  id='search-issue'
                  value={searchString}
                  onChange={(e) => setSearchQuery(e.target.value)}
                  startAdornment={
                    <InputAdornment position='start'>
                      <SearchIcon />
                    </InputAdornment>
                  }
                  endAdornment={
                    <>
                      {searchString ? (
                        <ClearIcon
                          onClick={() => clearQuery()}
                          sx={{ cursor: 'pointer' }}
                          color='inherit'
                        />
                      ) : null}
                    </>
                  }
                  label={t('search')}
                />
              </FormControl>
            </Box>

            <Box>
              <Button onClick={() => setAddModalOpen(true)} variant='contained'>
                {t('button-add_new')}
              </Button>
            </Box>
          </Box>

          <DataTable table='locations' columns={columns} rows={filteredData} />
        </StyledPaper>
      </Grid>
      <ModalDialog
        onCancel={() => setAddModalOpen(false)}
        open={addModalOpen}
        title={t('modal-add_location')}
        showActions={false}
      >
        <LocationForm
          action='create'
          onCancel={() => setAddModalOpen(false)}
          isShowCancel={true}
        />
      </ModalDialog>
      <ModalDialog
        onCancel={() => setEditModalOpen(false)}
        open={editModalOpen}
        title={t('modal-edit_location')}
        showActions={false}
      >
        {editedLocation && 
        <LocationForm
          action='update'
          onCancel={() => setEditModalOpen(false)}
          isShowCancel={true}
          location={editedLocation}
        />
      }
      </ModalDialog>
      <ModalDialog
        onCancel={() => setDeleteModalOpen(false)}
        onSubmit={() => onDeleteLocation()}
        open={deleteModalOpen}
        title={t('modal-delete_location')}
        showActions={true}
      >
        
        {t('delete')}
      </ModalDialog>
    </Grid>
  )
}

export default LocationsPage
