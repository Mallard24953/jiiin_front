/* eslint-disable @typescript-eslint/no-unused-vars */
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { object } from 'yup'
import { DevTool } from '@hookform/devtools'

import { ILocation } from '@/types'
import { useTranslation } from 'react-i18next'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import {
  Button,
  FormControl,
  FormHelperText,
  Grid,
  InputLabel,
  OutlinedInput,
} from '@mui/material'
import LoadingButton from '@mui/lab/LoadingButton'
import { IMaskInput } from 'react-imask'
import { forwardRef, useEffect, useState } from 'react'
import { useMutation, useQueryClient } from '@tanstack/react-query'
import { addLocationMutation, updateLocationMutation } from '../../api/locationsAPI'
import { useSnackbar } from 'notistack'

interface ILocationFormProps {
  isShowCancel?: boolean
  action: 'create' | 'update'
  onCancel: () => void
  location?: ILocation
}

interface IFormInput {
  name: string
  address: string
  phone: string
}

interface CustomProps {
  onChange: (event: { target: { name: string; value: string } }) => void
  name: string
}

const TextMaskCustom = forwardRef<HTMLInputElement, CustomProps>(
  function TextMaskCustom(props, ref) {
    const { onChange, ...other } = props
    return (
      <IMaskInput
        {...other}
        mask='+972#########'
        definitions={{
          '#': /[0-9]/,
        }}
        inputRef={ref}
        onAccept={(value: string) =>
          onChange({ target: { name: props.name, value } })
        }
        overwrite
      />
    )
  }
)

function LocationForm({ ...props }: ILocationFormProps) {
  const id = props.location?.id
  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation()
  const queryClient = useQueryClient()
  const addLocation = useMutation({
    mutationFn: addLocationMutation,
    retry: 3,
    onSuccess(data, variables, context) {
      queryClient.invalidateQueries({queryKey: ['locations']})
      props.onCancel()
      enqueueSnackbar(t('success'), {
        variant: "success",
      });
    },
    onError(error, variables, context) {
      enqueueSnackbar(t('error'), {
        variant: "error",
      });
    },
  })

  const updateLocation = useMutation({
    mutationFn: updateLocationMutation,
    retry: 3,
    onSuccess(data, variables, context) {
      queryClient.invalidateQueries({queryKey: ['locations']})
      props.onCancel()
    },
    onError(error, variables, context) {
        
    },
  })

  const [values, setValues] = useState({
    textmask: '+972',
  })

  useEffect(() => {
    if (props.location?.phone) {
      setValues({textmask: props.location?.phone as string})
    }
  }, [props.location])

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    })
  }

  const validationSchema = object()
    .shape({
      name: yup
        .string()
        .required(t('form-field.required'))
        .min(3, 'Минимум 3 символа')
        .max(255),
      address: yup
        .string()
        .required(t('form-field.required'))
        .min(3, 'Минимум 3 символа')
        .max(255),
      phone: yup
        .string()
        .required(t('form-field.required'))
        .length(13, t('phone-wrong')),
    })
    .required()

  const methods = useForm<IFormInput>({
    resolver: yupResolver(validationSchema),
    mode: 'onChange',
    defaultValues: {
      name: props.location?.name,
      address: props.location?.address,
      phone: props.location?.phone,
    },
  })

  const { handleSubmit, control, formState } = methods
  const errors = formState.errors

  const onSubmit: SubmitHandler<IFormInput> = (data) => {
    if (props.action === 'create') {
      addLocation.mutate(data)
    } else {
      updateLocation.mutate({id, ...data})      
    }
  }

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Grid
          sx={{ marginTop: '0px' }}
          container
          rowSpacing={2}
          columnSpacing={3}
        >
          <Grid item xs={12}>
            <Controller
              name='name'
              control={control}
              render={({ field }) => (
                <FormControl
                  required
                  error={errors.name?.message ? true : false}
                  sx={{ width: '100%' }}
                  variant='outlined'
                  {...field}
                  size='small'
                >
                  <InputLabel htmlFor='name'>{t('name')}</InputLabel>
                  <OutlinedInput
                    {...field}
                    size='small'
                    id='name'
                    label={t('name')}
                  />
                  {!errors.name?.message && (
                    <FormHelperText>{t('form-field.required')}</FormHelperText>
                  )}
                  {errors.name?.message && (
                    <FormHelperText>{errors.name?.message}</FormHelperText>
                  )}
                </FormControl>
              )}
            />
          </Grid>

          <Grid item xs={12}>
            <Controller
              name='address'
              control={control}
              render={({ field }) => (
                <FormControl
                  required
                  error={errors.address?.message ? true : false}
                  sx={{ width: '100%' }}
                  variant='outlined'
                  {...field}
                  size='small'
                >
                  <InputLabel htmlFor='address'>{t('address')}</InputLabel>
                  <OutlinedInput
                    {...field}
                    size='small'
                    id='address'
                    label={t('address')}
                  />
                  {!errors.address?.message && (
                    <FormHelperText>{t('form-field.required')}</FormHelperText>
                  )}
                  {errors.address?.message && (
                    <FormHelperText>{errors.address?.message}</FormHelperText>
                  )}
                </FormControl>
              )}
            />
          </Grid>

          <Grid item xs={12}>
            <Controller
              name='phone'
              control={control}
              render={({ field }) => (
                <FormControl
                  dir='ltr'
                  required
                  error={errors.phone?.message ? true : false}
                  sx={{ width: '100%' }}
                  variant='outlined'
                  {...field}
                  size='small'
                >
                  <InputLabel htmlFor='phone-masked'>{t('phone')}</InputLabel>
                  <OutlinedInput
                    {...field}
                    sx={{ textAlign: 'right' }}
                    value={values.textmask}
                    onChange={handleChange}
                    name='textmask'
                    size='small'
                    id='phone-masked'
                    label={t('phone')}
                    inputComponent={
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      TextMaskCustom as any
                    }
                  />
                  {!errors.phone?.message && (
                    <FormHelperText>{t('form-field.required')}</FormHelperText>
                  )}
                  {errors.phone?.message && (
                    <FormHelperText>{errors.phone?.message}</FormHelperText>
                  )}
                </FormControl>
              )}
            />
          </Grid>

          <Grid
            item
            xs={12}
            sx={{ display: 'flex', justifyContent: 'end', gap: 2 }}
          >
            {props.isShowCancel && (
              <Button variant='outlined' onClick={() => props.onCancel()}>
                {t('button-cancel_label')}
              </Button>
            )}

            <LoadingButton
              disabled={!formState.isValid}
              variant='contained'
              type='submit'
              loading={false}
            >
              {t('button-save_label')}
            </LoadingButton>
          </Grid>
        </Grid>
      </form>
      <DevTool control={control} /> 
    </>
  )
}

export default LocationForm
