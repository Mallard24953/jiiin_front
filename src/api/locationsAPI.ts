/* eslint-disable @typescript-eslint/no-unused-vars */
import { ILocation } from '@/types'
import axiosAPI from '../utils/axios'

const getLocationsQuery = async (): Promise<ILocation[]> => {
  const { data } = await axiosAPI.get('/locations')
  return data
}

const getLocationByIDQuery = async (id: string): Promise<ILocation> => {
  const { data } = await axiosAPI.get(`/locations/${id}`)
  return data
}

const addLocationMutation = async (data: ILocation) => {
  const res = await axiosAPI.post('/locations', data)
  return res
}

const updateLocationMutation = async (data: ILocation) => {
  const res = await axiosAPI.patch(`/locations/${data.id}`, data)
  return res
}

const deleteLocationMutation = async (id: string) => {
  const res = await axiosAPI.delete(`/locations/${id}`)
  return res
}

export {
  getLocationsQuery,
  addLocationMutation,
  updateLocationMutation,
  deleteLocationMutation,
  getLocationByIDQuery,
}
